package students;

public class CurriculumItem {
  public String name;
  public int hours;

  public CurriculumItem(String aName, int aHours) {
    name = aName;
    hours = aHours;
  }

  public String toString() {
    return String.format("{%s:%d}", name, hours);
  }
}
