package students;

import java.time.LocalDate;
import java.util.Optional;
import java.util.stream.Stream;

public class Pipeline {
  public enum SortMode {
    SORT_NONE, SORT_MARK, SORT_DATE
  }

  public enum FilterMode {
    FILTER_NONE, FILTER_MARK
  }

  static String formatOutputAtDate(Student s, LocalDate atDate) {
    String curriculumName = s.curriculum.name;
    long hoursLeft = s.hoursLeft(atDate);
    String endDate = s.lastDayOfCurriculum().toString();

    Optional<Double> avg = s.averageMark();
    String avgAsString;
    if (avg.isPresent()) {
      avgAsString = String.format("%.2f", avg.get());
    } else {
      avgAsString = "N/A";
    }

    String maxAvgString;
    double possibleMaxAverage = s.possibleMaxAvarage(atDate);
    if (possibleMaxAverage >= 4.5) {
      maxAvgString =
          String.format("Может продолжать обучение(достижимое среднее %.2f)", possibleMaxAverage);
    } else {
      maxAvgString = String.format("Отчислить (достижимое среднее %.2f)", possibleMaxAverage);
    }

    String output = String.format("%s; %s; Осталось %dч. (по %s); Средний бал: %s; %s", s.name,
        curriculumName, hoursLeft, endDate, avgAsString, maxAvgString);
    return output;
  }

  static int optionalDoubleComparator(Optional<Double> a, Optional<Double> b) {
    if (a.isPresent() && b.isPresent()) {
      return Double.compare(a.get(), b.get());
    }
    if (!a.isPresent() && b.isPresent()) {
      return -1;
    }
    if (a.isPresent() && !b.isPresent()) {
      return 1;
    }
    return 0;
  }

  static Stream<String> create(Stream<Student> stream, LocalDate atDate, SortMode sortMode,
      FilterMode filterMode) {

    Stream<Student> filtered;
    switch (filterMode) {
      case FILTER_MARK:
        filtered = stream.filter(
            (student) -> student.possibleMaxAvarage(atDate) >= 4.5
        );
        break;
      default:
        filtered = stream;
    }

    Stream<Student> sorted;
    switch (sortMode) {
      case SORT_MARK:
        sorted =
            filtered.sorted(
                (a, b) -> optionalDoubleComparator(a.averageMark(), b.averageMark())
            );
        break;
      case SORT_DATE:
        sorted =
            filtered.sorted(
                (a, b) -> a.lastDayOfCurriculum().compareTo(b.lastDayOfCurriculum())
            );
        break;
      default:
        sorted = filtered;
    }

    Stream<String> formatter = sorted.map(
            (student) -> formatOutputAtDate(student, atDate)
    );
    return formatter;
  }
}
