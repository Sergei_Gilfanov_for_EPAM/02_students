package students;

import java.util.List;

public class Curriculum {
  public String name;
  private List<CurriculumItem> items;
  // Рабочих часов в дне
  public static final double HOURS_IN_DAY = 8.0;

  Curriculum(String aName, List<CurriculumItem> aItems) {
    name = aName;
    items = aItems;
  }

  public int lengthInDays() {
    int totalHours = 0;
    for (CurriculumItem i : items) {
      totalHours += i.hours;
    }
    // Последний день может оказаться заполненным не до конца, но все равно его считаем
    return (int) java.lang.Math.ceil(totalHours / HOURS_IN_DAY);
  }

  public int lengthInHours() {
    int totalHours = 0;
    for (CurriculumItem i : items) {
      totalHours += i.hours;
    }
    return totalHours;
  }
}
