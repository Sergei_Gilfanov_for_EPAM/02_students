package students;

import java.io.IOException;
import java.io.StringWriter;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.stream.Stream;
import org.apache.commons.cli.*;

public class Main {
  public void boot(String[] args) throws IOException {
    String sortStr;
    Option sortOpt = Option.builder("sort")
        .argName("тип сортировки")
        .required(false)
        .hasArg()
        .desc("Как сортировать выводимые данные: mark|date")
        .build();

    String filterStr;
    Option filterOpt = Option.builder("filter")
        .argName("тип фильтрации")
        .required(false)
        .hasArg()
        .desc("Как сортировать выводимые данные: mark")
        .build();


    Options options = new Options();
    options.addOption(sortOpt);
    options.addOption(filterOpt);

    CommandLineParser parser = new DefaultParser();

    try {
      CommandLine line = parser.parse(options, args);
      sortStr = line.getOptionValue("sort");
      filterStr = line.getOptionValue("filter");
    } catch (ParseException e) {
      HelpFormatter formatter = new HelpFormatter();
      // Шаманство. formater.printHelp, если позволить ему
      // писать в System.out самому, полностью игнорирует кодировку
      // Поэтому сначала мы перенаправляем его вывод в строку,
      // а потому уже эту строку печатаем.
      StringWriter sw = new StringWriter();
      PrintWriter pw = new PrintWriter(sw);
      formatter.printHelp(pw, 80, "02_students", "", options, 4, 2, "");
      pw.flush();
      System.out.print(sw.toString());
      return;
    }

    Pipeline.SortMode sortMode;
    if (sortStr != null) {
      switch (sortStr) {
        case "mark":
          sortMode = Pipeline.SortMode.SORT_MARK;
          break;
        case "date":
          sortMode = Pipeline.SortMode.SORT_DATE;
          break;
        default:
          sortMode = Pipeline.SortMode.SORT_NONE;
      }
    } else {
      sortMode = Pipeline.SortMode.SORT_NONE;
    }

    Pipeline.FilterMode filterMode;
    if (filterStr != null) {
      switch (filterStr) {
        case "mark":
          filterMode = Pipeline.FilterMode.FILTER_MARK;
          break;
        default:
          filterMode = Pipeline.FilterMode.FILTER_NONE;
      }
    } else {
      filterMode = Pipeline.FilterMode.FILTER_NONE;
    }

    LocalDate atDate = LocalDate.of(2016, 04, 06);
    Stream<Student> studentsStream = StudentsData.data.stream();

    // sortMode = Pipeline.SortMode.SORT_MARK;
    // sortMode = Pipeline.SortMode.SORT_DATE;

    filterMode = Pipeline.FilterMode.FILTER_MARK;

    Pipeline.create(studentsStream, atDate, sortMode, filterMode).forEach(System.out::println);
  }

  public static void main(String[] args) throws IOException {
    new Main().boot(args);
  }
}
