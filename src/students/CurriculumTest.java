package students;

import static org.junit.Assert.*;
import java.util.Arrays;
import org.junit.Test;

public class CurriculumTest {

  @Test
  public void testLengthInDaysPartial() {
    Curriculum curriculum = new Curriculum("Curriculum 0",
        Arrays.asList(
            new CurriculumItem("Curriculum 0/Course 1", 8),
            new CurriculumItem("Curriculum 0/Course 2", 4),
            new CurriculumItem("Curriculum 0/Course 3", 8)
        )
    );
    assertTrue("Partial day", curriculum.lengthInDays() == 3);
  }

  @Test
  public void testLengthInDaysFull() {
    Curriculum curriculum = new Curriculum("Curriculum 0",
        Arrays.asList(
            new CurriculumItem("Curriculum 0/Course 1", 8),
            new CurriculumItem("Curriculum 0/Course 2", 8),
            new CurriculumItem("Curriculum 0/Course 3", 8)
        )
    );
    assertTrue("Partial day", curriculum.lengthInDays() == 3);
  }


  @Test
  public void testLengthInHours() {
    Curriculum curriculum = new Curriculum("Curriculum 0",
        Arrays.asList(
            new CurriculumItem("Curriculum 0/Course 1", 8),
            new CurriculumItem("Curriculum 0/Course 2", 4),
            new CurriculumItem("Curriculum 0/Course 3", 8)
        )
    );
    assertTrue("Partial day", curriculum.lengthInHours() == 20);
  }

}
