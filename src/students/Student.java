package students;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

public class Student {
  String name;
  Curriculum curriculum;
  LocalDate startDate;
  List<Integer> marks;

  public Student(String aName, Curriculum aCurriculum, LocalDate aStartDate, List<Integer> aMarks) {
    name = aName;
    curriculum = aCurriculum;
    if (aMarks.size() > aCurriculum.lengthInDays()) {
      throw new IndexOutOfBoundsException("More marks then days in curriculum");
    }
    startDate = aStartDate;
    marks = aMarks;
  }

  Optional<Double> averageMark() {
    int size = marks.size();
    if (size == 0) {
      return Optional.empty();
    }
    double total = 0;
    for (int currentMark : marks) {
      total += currentMark;
    }
    return Optional.of(total / size);
  }

  // Исходим из предополжения, что все оценки, включая на дату atDate, уже поставлены
  double possibleMaxAvarage(LocalDate atDate) {
    double total = 0;
    for (int currentMark : marks) {
      total += currentMark;
    }

    int curriculumDays = curriculum.lengthInDays();
    LocalDate lastDayOfCurriculum = startDate.plusDays(curriculumDays);
    long daysLeft = ChronoUnit.DAYS.between(atDate, lastDayOfCurriculum);
    long marksLeft = curriculumDays - marks.size(); // Сколько всего не поставленных оценок
    long daysToMark = Math.min(daysLeft, marksLeft); // И сколько можно успеть поставить
    long maxMarksCount; // Максимальное количество оценок(включая прошлые), которое можно успеть
                        // получить
    if (daysToMark > 0) {
      // Самый лучший для студента случай - во все оставшиеся дни он получит 5
      total += 5 * daysToMark;
      maxMarksCount = marks.size() + daysToMark;
    } else {
      maxMarksCount = marks.size();
    }
    return total / maxMarksCount;
  }

  long hoursLeft(LocalDate atDate) {
    long daysPassed = ChronoUnit.DAYS.between(startDate, atDate) + 1;
    return Math.max(0, curriculum.lengthInHours() - (long) Curriculum.HOURS_IN_DAY * daysPassed);
  }

  LocalDate lastDayOfCurriculum() {
    return startDate.plusDays(curriculum.lengthInDays() - 1);
  }
}
