package students;

import java.util.List;
import java.time.LocalDate;
import java.util.Arrays;

public class StudentsData {
  static private List<Curriculum> curriculums = Arrays.asList(
      new Curriculum("Curriculum 0",
          Arrays.asList(
              new CurriculumItem("Curriculum 0/Course 0", 8),
              new CurriculumItem("Curriculum 0/Course 1", 8),
              new CurriculumItem("Curriculum 1/Course 2", 4),
              new CurriculumItem("Curriculum 1/Course 3", 8)
          )
      ),
      new Curriculum("Curriculum 1",
          Arrays.asList(
              new CurriculumItem("Curriculum 1/Course 0", 8),
              new CurriculumItem("Curriculum 1/Course 1", 8),
              new CurriculumItem("Curriculum 1/Course 2", 8),
              new CurriculumItem("Curriculum 1/Course 3", 8)
          )
      ),
      new Curriculum("Curriculum 2",
          Arrays.asList(
              new CurriculumItem("Curriculum 1/Course 0", 8),
              new CurriculumItem("Curriculum 1/Course 1", 8),
              new CurriculumItem("Curriculum 1/Course 2", 8),
              new CurriculumItem("Curriculum 1/Course 3", 8)
          )
      )
  );
  
  static List<Student> data = Arrays.asList(
      new Student("Student 5", curriculums.get(2), LocalDate.of(2016, 04, 06), Arrays.asList(4)),
      new Student("Student 3", curriculums.get(1), LocalDate.of(2016, 04, 04), Arrays.asList(4,4,4)),
      new Student("Student 2", curriculums.get(1), LocalDate.of(2016, 04, 03), Arrays.asList(4,4,5,5)),
      new Student("Student 0", curriculums.get(0), LocalDate.of(2016, 04, 01), Arrays.asList(4,4,5,5)),
      new Student("Student 4", curriculums.get(2), LocalDate.of(2016, 04, 05), Arrays.asList(4,4)),
      new Student("Student 1", curriculums.get(0), LocalDate.of(2016, 04, 02), Arrays.asList(4,4,4,5)),
      new Student("Student 6", curriculums.get(2), LocalDate.of(2016, 04, 07), Arrays.asList(new Integer[0]))
  );
}
