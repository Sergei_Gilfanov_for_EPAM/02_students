package students;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Optional;
import org.junit.Test;

public class StudentTest {

  @Test
  public void testAverageNoMarks() {
    Student s = new Student("Student 0",
        new Curriculum("Curriculum 0",
            Arrays.asList(
                new CurriculumItem("Curriculum 0/Course 1", 1)
            )
        ),
        LocalDate.of(2016, 04, 01),
        Arrays.asList(new Integer[0])
    );

    Optional<Double> avg = s.averageMark();
    assertFalse("No marks - no average", avg.isPresent());
  }

  @Test
  public void testAverageMarks() {
    Student s = new Student("Student 0",
        new Curriculum("Curriculum 0",
            Arrays.asList(
                new CurriculumItem("Curriculum 0/Course 1", 8),
                new CurriculumItem("Curriculum 0/Course 2", 8)
            )
        ),
        LocalDate.of(2016, 04, 01),
        Arrays.asList(1, 3)
    );

    Optional<Double> avg = s.averageMark();
    assertTrue("No marks - no average", avg.isPresent() && avg.get() == (1.0 + 3.0) / 2);
  }

  @Test
  public void testPossibleMaxAverageNoMarks() {
    Student s = new Student("Student 0",
        new Curriculum("Curriculum 0",
            Arrays.asList(
                new CurriculumItem("Curriculum 0/Course 1", 8),
                new CurriculumItem("Curriculum 0/Course 2", 4),
                new CurriculumItem("Curriculum 0/Course 3", 8)
            )
        ),
        LocalDate.of(2016, 04, 01),
        Arrays.asList(new Integer[0])
    );

    double avg = s.possibleMaxAvarage(LocalDate.of(2016, 04, 02));
    assertTrue("", avg == 5.0);
    // Тут имеем дырку в условиях задачи. По тексту получается, что достаточно получить одну пятерку
    // в последний день - и среднее будет равно 5.
  }

  @Test
  public void testPossibleMaxAverageAllMarks() {
    Student s = new Student("Student 0",
        new Curriculum("Curriculum 0",
            Arrays.asList(
                new CurriculumItem("Curriculum 0/Course 1", 8),
                new CurriculumItem("Curriculum 0/Course 2", 4),
                new CurriculumItem("Curriculum 0/Course 3", 8)
            )
        ),
        LocalDate.of(2016, 04, 01),
        Arrays.asList(1, 2)
    );

    double avg = s.possibleMaxAvarage(LocalDate.of(2016, 04, 02));
    assertTrue("", avg == (1.0 + 2.0 + 5.0) / 3);
  }

  @Test(expected = IndexOutOfBoundsException.class)
  public void testTooManyMarks() {
    Student s = new Student("Student 0",
        new Curriculum("Curriculum 0",
            Arrays.asList(
                new CurriculumItem("Curriculum 0/Course 1", 8),
                new CurriculumItem("Curriculum 0/Course 1", 8)
            )
        ),
        LocalDate.of(2016, 04, 01),
        Arrays.asList(2, 3, 4)
    );

    double avg = s.possibleMaxAvarage(LocalDate.of(2016, 04, 02));
    assertTrue("", avg == (2.0 + 5.0) / 2);
  }

  @Test
  public void testHoursLeft() {
    Student s = new Student("Student 0",
        new Curriculum("Curriculum 0",
            Arrays.asList(
                new CurriculumItem("Curriculum 0/Course 1", 8),
                new CurriculumItem("Curriculum 0/Course 2", 4),
                new CurriculumItem("Curriculum 0/Course 3", 8)
            )
        ),
        LocalDate.of(2016, 04, 01),
        Arrays.asList(1, 2)
    );

    long hoursLeft = s.hoursLeft(LocalDate.of(2016, 04, 02));
    assertTrue("", hoursLeft == 4);
  }
}
